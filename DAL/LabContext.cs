using System;
using JetBrains.Annotations;
using Lab7.Models;
using Microsoft.EntityFrameworkCore;

namespace Lab7.DAL
{
    public class LabContext : DbContext
    {
        public LabContext(DbContextOptions<LabContext> options) : base(options)
        {
            this.Database.EnsureCreated();
        }

        protected LabContext()
        {
        }


        public DbSet<User> Users {get;set;}

        public DbSet<Company> Companies {get;set;}
    }
}