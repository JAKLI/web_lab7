using System;
using Lab7.Models;

namespace Lab7.DAL
{
    public static class DbInitializer
    {
        public static void Initialize(LabContext context)
        {
            if(!context.Database.EnsureCreated())
            {
                var companyId = context.Companies.Add(new Company
            {
                Name = "BestCompany"
            }).Entity.Id;

            context.Users.Add(new User
            {
                Name = "Some",
                Surname = "User",
                Birthday = DateTime.Parse("2000/01/01"),
                CompanyId = companyId
            });
            context.Users.Add(new User
            {
                Name = "Some",
                Surname = "UserWithOutCompany",
                Birthday = DateTime.Parse("2000/01/01")
            });
            context.SaveChanges();
            }
            
        }
    }
}