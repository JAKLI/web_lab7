using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab7.DAL;
using Lab7.Models;
using Lab7.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace Lab7.Controllers
{
    [EnableCors("AllowAll")]
    [Route("api/Company/[action]")]
    public class CompanyController:Controller
    {
        private readonly LabContext _context;

        public CompanyController(LabContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<CompanyViewModel> GetAll()
        {
            return _context.Companies.Select(c=>new CompanyViewModel{
                Id = c.Id,
                Name = c.Name,
                OpenDate = c.OpenDate
            }).ToList();
        }

        // GET api/values/5
        [HttpGet("{id}",Name = "GetCompany")]
        public IActionResult GetById(int id)
        {
            var company = _context.Companies.FirstOrDefault(u=>u.Id == id);
            if(company == null)
                return NotFound();
            
            return new ObjectResult(BuildCompanyViewModelFromCompany(company));
        }

        [HttpGet("{id}")]
        public IActionResult GetCompanyUsers(int id)
        {
            var company = _context.Companies.FirstOrDefault(t => t.Id == id);
            if (company == null)
            {
                return NotFound();
            }

            var users = _context.Companies.Where(c => c.Id == id).Join(_context.Users,c=>c.Id,u=>u.CompanyId,(c,u)=>u)
            .Select(u=>new UserViewModel{
                Id = u.Id,
                Name = u.Name,
                Surname = u.Surname,
                Email = u.Email,
                Birthday = u.Birthday,
                CompanyId = u.CompanyId
            });
            
            return new ObjectResult(users);
        }

        [HttpGet("{req}")]
        public IEnumerable<CompanyViewModel> FindCompanies(string req)
        {
            var companies = _context.Companies.Where(u => u.Name.Contains(req) || u.OpenDate.ToString().Contains(req))
            .Distinct()
            .Select(c=>new CompanyViewModel{
                Id = c.Id,
                Name = c.Name,
                OpenDate = c.OpenDate
            }).ToList();

            return companies;
        }

        

        // POST api/values
        [HttpPost]
        public IActionResult Create([FromBody]CompanyViewModel company)
        {
            if(company == null){
                return BadRequest();
            }

            var newCompany = _context. Companies.Add(new Company{
                Name = company.Name,
                OpenDate = company.OpenDate
            }).Entity;
            _context.SaveChanges(); 

            return CreatedAtRoute("GetCompany", new {id = company.Id},
            BuildCompanyViewModelFromCompany(newCompany));
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]CompanyViewModel company)
        {
            if (company == null || company.Id != id)
            {
                return BadRequest();
            }

            var oldCompany = _context.Companies.FirstOrDefault(t => t.Id == id);
            if (oldCompany == null)
            {
                return NotFound();
            }

            
            oldCompany.Name = company.Name;
            oldCompany.OpenDate = company.OpenDate;

            _context.Companies.Update(oldCompany);
            _context.SaveChanges();

            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var company = _context.Companies.FirstOrDefault(t => t.Id == id);
            if (company == null)
            {
                return NotFound();
            }

            _context.Companies.Remove(company);
            _context.SaveChanges();
            
            return new NoContentResult();
        }

        private CompanyViewModel BuildCompanyViewModelFromCompany(Company comapny)
        {
            return new CompanyViewModel
            {
                Id = comapny.Id,
                Name = comapny.Name,
                OpenDate = comapny.OpenDate
            };
        }
    }
}