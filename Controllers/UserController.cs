﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab7.DAL;
using Lab7.Models;
using Lab7.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace Lab7.Controllers
{
    [EnableCors("AllowAll")]
    [Route("api/User/[action]")]
    public class UserController : Controller
    {

        private readonly LabContext _context;

        public UserController(LabContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<UserViewModel> GetAll()
        {
            return _context.Users.Select(u=> new UserViewModel{
                Id = u.Id,
                Name = u.Name,
                Surname = u.Surname,
                Email = u.Email,
                Birthday = u.Birthday,
                CompanyId = u.CompanyId
            }).ToList();
        }

        // GET api/values/5
        [HttpGet("{id}",Name = "GetUser")]
        public IActionResult GetById(int id)
        {
            var user = _context.Users.FirstOrDefault(u=>u.Id == id);
            if(user == null)
                return NotFound();

            return new ObjectResult(BuildUserViewModelFromUser(user));
        }

        
        [HttpGet("{req}")]
        public IEnumerable<UserViewModel> FindUsers(string req)
        {
            var users = _context.Users.Where(u => u.Name.Contains(req) || u.Surname.Contains(req) || u.Birthday.ToString().Contains(req)).Distinct()
            .Select(u=>new UserViewModel{
                Id = u.Id,
                Name = u.Name,
                Surname = u.Surname,
                Email = u.Email,
                Birthday = u.Birthday,
                CompanyId = u.CompanyId
            }).ToList();
            return users;
        }

        // POST api/values
        [HttpPost]
        public IActionResult Create([FromBody]UserViewModel user)
        {
            if(user == null){
                return BadRequest();
            }

            var newUser = _context.Users.Add(BuildUserFromUserViewModel(user)).Entity;
            _context.SaveChanges(); 

            return CreatedAtRoute("GetUser", new {id = user.Id},BuildUserViewModelFromUser(newUser));
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]User user)
        {
            if (user == null || user.Id != id)
            {
                return BadRequest();
            }

            var oldUser = _context.Users.FirstOrDefault(t => t.Id == id);
            if (oldUser == null)
            {
                return NotFound();
            }
            
            oldUser.Name = user.Name;
            oldUser.Name = user.Name;
            oldUser.Surname = user.Surname;
            oldUser.Email = user.Email;
            oldUser.Birthday = user.Birthday;
            oldUser.CompanyId = user.CompanyId;

            _context.Users.Update(oldUser);
            _context.SaveChanges();

            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = _context.Users.FirstOrDefault(t => t.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            _context.SaveChanges();
            
            return new NoContentResult();
        }

        private User BuildUserFromUserViewModel(UserViewModel userViewModel)
        {
            return new User
            {
                Id = userViewModel.Id,
                Name = userViewModel.Name,
                Surname = userViewModel.Surname,
                Email = userViewModel.Email,
                Birthday = userViewModel.Birthday,
                CompanyId = userViewModel.CompanyId
            };
        }

        private UserViewModel BuildUserViewModelFromUser(User user)
        {
            return new UserViewModel
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname,
                Email = user.Email,
                Birthday = user.Birthday,
                CompanyId = user.CompanyId
            };
        }
    }
}
