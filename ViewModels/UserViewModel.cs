using System;

namespace Lab7.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email{ get;set; }
        public DateTime Birthday { get; set; }
        public int? CompanyId { get; set; }
    }
}