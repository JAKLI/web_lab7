using System;
namespace Lab7.Models
{
    public class User:Entity
    {
        public string Name{get;set;}
        public string Surname{ get; set; }
        public string Email { get; set; }
        public int? CompanyId{ get; set; }
        public DateTime Birthday { get; set; }
        public virtual Company Company{ get; set; }
    }
}