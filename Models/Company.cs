using System;
using System.Collections.Generic;

namespace Lab7.Models
{
    public class Company:Entity
    {
        public string Name{get;set;}

        public DateTime OpenDate{ get; set; }
        
        public virtual ICollection<User> Users{ get;set; }
    }
}